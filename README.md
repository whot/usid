Universal Stylus Interface Daemon
=================================

USID is a DBus daemon that provides USI-compatible configuration for styli
that can be uniquely identified by the system by a serial number.

The USI is a part of the [HID protocol](https://usb.org/sites/default/files/hut1_22.pdf) (p166)
and specifies three properties on compatible devices:

- **Preferred Color**: the 8-bit Web color or 24 bit RGB color assigned to the device
- **Preferred Line Width**: the physical width (e.g. 2mm)
- **Preferred Line Style**: One of Ink, Pencil, Highlighter, Chisel Marker, Brush, No Preference.

Each of these properties has an additional HID item to specify whether the
property is readonly or writable.

An USI compatible application would query the device and match the device's
virtual color, width and style. The physical device thus behaves identical
across applications and systems.

USID is a managing daemon for USI-compatible styli. Rather than querying the
device directly (which requires elevated privileges) an application queries
USID over DBus for the properties.

USID can obtain the required information from the device, or from local
storage. In the latter case, any tool with a unique serial can be treated like
a USI-compatible stylus.
