#!/usr/bin/env python3
#
# SPDX-License-Identifier: MIT
#
# This file is formatted with Python Black
#

from dbus_next import PropertyAccess
from dbus_next.aio import MessageBus
from dbus_next.service import ServiceInterface, method, dbus_property

from typing import List
from itertools import count

import attr
import asyncio
import enum

INTF_NAME = "org.freedesktop.usid0"

counter = count()  # for stylus object paths


def intf(suffix: str):
    return f"{INTF_NAME}.{suffix}"


def objpath(suffix: str):
    return f"/{INTF_NAME}{'.' if suffix else ''}{suffix}".replace(".", "/")


def int8_validator(instance, attribute, value):
    if not isinstance(value, int) or not 0 <= value <= 0xFF:
        raise ValueError("Invalid value, must be 0-0xff")


class LineStyle(enum.IntEnum):
    INK = 0x72
    PENCIL = 0x73
    HIGHLIGHTER = 0x74
    CHISEL_MARKER = 0x75
    BRUSH = 0x76
    NO_PREFERENCE = 0x77


@attr.s
class Color(object):
    red: int = attr.ib(validator=int8_validator)
    green: int = attr.ib(validator=int8_validator)
    blue: int = attr.ib(validator=int8_validator)


@attr.s
class Width(object):
    width: int = attr.ib()
    max: int = attr.ib()
    resolution: int = attr.ib()


@attr.s
class Stylus(ServiceInterface):
    bus: MessageBus = attr.ib()
    serial: int = attr.ib()
    color: Color = attr.ib()
    width: Width = attr.ib()
    style: LineStyle = attr.ib(attr.validators.in_(list(LineStyle)))
    is_usi: bool = attr.ib(default=False)
    objpath: str = attr.ib(
        init=False, default=attr.Factory(lambda: objpath(f"s{next(counter)}"))
    )

    def __attrs_pre_init__(self):
        super().__init__(intf("Stylus"))

    def __attrs_post_init__(self):
        self.bus.export(self.objpath, self)

    @dbus_property(access=PropertyAccess.READ)
    def Serial(self) -> "u":  # type: ignore
        """
        The serial number of this stylus
        """
        return self.serial

    @dbus_property()
    def Color(self) -> "(uuu)":  # type: ignore
        """
        The RGB color value of this color.

        The HID Spec allows for an either 8bit web color (see lookup table on
        p 314) or a 24 bit RGB value. We only do the latter for now.
        """
        return [self.color.red, self.color.green, self.color.blue]

    @Color.setter
    def Color(self, color: "(uuu)"):  # type: ignore
        self.color = Color(*color)

    @dbus_property()
    def Style(self) -> "u":  # type: ignore
        return self.style.value

    @Style.setter
    def Style(self, style: "u"):  # type: ignore
        self.style = LineStyle(style)

    @dbus_property(access=PropertyAccess.READ)
    def MaxWidth(self) -> "u":  # type: ignore
        """
        The maximum width of the stylus, in micrometers
        """
        return self.width.max

    @dbus_property(access=PropertyAccess.READ)
    def Resolution(self) -> "u":  # type: ignore
        """
        The resolution for the width in units per micrometer
        """
        return self.width.resolution

    @dbus_property()
    def Width(self) -> "u":  # type: ignore
        """
        The width in micrometers
        """
        return self.width.resolution

    @Width.setter
    def Width(self, width: "u"):  # type: ignore
        self.width.width = width

    @dbus_property(access=PropertyAccess.READ)
    def IsUsi(self) -> "b":  # type: ignore
        """
        True if this device is an actual USI pen and can store configuration
        on the device.
        """
        return self.is_usi


@attr.s
class Usid(ServiceInterface):
    bus: MessageBus = attr.ib()
    styli: List[Stylus] = attr.ib(factory=list)

    def __attrs_pre_init__(self):
        super().__init__(intf("Usid"))

    def __attrs_post_init__(self):
        self.bus.export(objpath(""), self)

    @dbus_property(access=PropertyAccess.READ)
    def Styli(self) -> "ao":  # type: ignore
        """
        This property is used for debugging and should not usually be used by
        an application.
        """
        return [s.objpath for s in self.styli]

    @method()
    def LookupStylus(self, serial: "u") -> "ao":
        """
        Return an array with either zero or one object path that represents
        the stylus with the given serial number. If the array is empty, no
        stylus with that serial number is known.
        """
        return [s.objpath for s in self.styli if s.serial == serial]


async def start():
    # FIXME: currently on session path but to access USI devices we probably
    # should run on the system bus
    bus = await MessageBus().connect()
    test_styli = [
        Stylus(
            bus,
            serial=0x123,
            color=Color(0, 0, 0),
            width=Width(1, 10, 100),
            style=LineStyle.CHISEL_MARKER,
        ),
        Stylus(
            bus,
            serial=0xABC,
            color=Color(255, 0, 0),
            width=Width(2, 20, 100),
            style=LineStyle.PENCIL,
        ),
    ]
    Usid(bus, styli=test_styli)
    await bus.request_name(INTF_NAME)
    await bus.wait_for_disconnect()


def main():
    asyncio.run(start())


if __name__ == "__main__":
    main()
